
LOAD CLASSES ../jars/voltdb-fantasy-sports-registration.jar;

CREATE TABLE customer
(customer_id BIGINT NOT NULL PRIMARY KEY
,customer_name VARCHAR(100) NOT NULL);

PARTITION TABLE customer ON COLUMN customer_id;

CREATE TABLE contest
(contest_id BIGINT NOT NULL PRIMARY KEY
,max_entrants INTEGER NOT NULL
,start_register TIMESTAMP
,end_register TIMESTAMP);

PARTITION TABLE contest ON COLUMN contest_id;

CREATE TABLE contest_entry
(contest_id BIGINT NOT NULL 
,customer_id BIGINT NOT NULL
,entrydate TIMESTAMP NOT NULL
,PRIMARY KEY (contest_id,customer_id));

PARTITION TABLE contest_entry ON COLUMN contest_id;

CREATE VIEW contest_entry_count AS
SELECT contest_id, count(*) current_entries 
FROM contest_entry
GROUP BY contest_id;

CREATE PROCEDURE 
PARTITION ON TABLE contest_entry COLUMN contest_id
FROM CLASS server.RegisterForCompetition;

CREATE PROCEDURE GetAllContests AS
select c.contest_id, c.max_entrants, e.current_entries, c.start_register, c.end_register from contest c, contest_entry_count e
where e.contest_id = c.contest_id order by contest_id;



