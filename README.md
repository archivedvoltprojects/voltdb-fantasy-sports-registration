# voltdb-fantasy-sports-registration

A demonstration of how you can use VoltDB to handle very large volumes of simultaneous 
requests for a shared, finite resource, in this case the right to enter a fantasy sports 
competition of an exact size.

## Installation

### Download and install the eval version of VoltDB

See: https://www.voltdb.com/try-voltdb/download-enterprise/

If you want to use an AWS cluster email us - we have cloudformation scripts available.

### Download this code

````
git clone https://sr_mad_science@bitbucket.org/voltdbseteam/voltdb-fantasy-sports-registration.git
````

### Create the database

 cd to the ddl directory and create the database. In the example below the database lives on three servers called vdb1, vdb2 and vdb3:
 
````
cd ddl
sqlcmd --servers=vdb1 < ddl.sql
````
  
### Launch the web gui 

VoltDB automatically runs a GUI on port 8080 of each host it's running on:

````
http://vdb1:8080/
````

### Run the script 

1. There are two ways to run the script. If left alone or just given a list of hostnames it will do 2 tests

    1. One for 16 contests, each with 40,000 places
    1. One for a single contest, with 1 million places.

In each of these cases we start at 5KTPS and keep increasing the workload in 5K TPS increments until the 95th percentile latency is greater than 5ms.

2. Or you can specify parameters, which will do a single run with the entered values:

````
sh rundemo.sh hostname contestcount customercount tpms secondspertest placesineachcontest maxlatency logfilename
````

Either way the script will produce an output file showing what happened.

## An example on AWS

For testing purposes we ran the 2 default tests above on the following configuration:

* AWS 
* [z1d.xlarge](https://aws.amazon.com/ec2/instance-types/z1d/) or [z1d.2xlarge](https://aws.amazon.com/ec2/instance-types/z1d/)
* 3 nodes
* k=1 (1 spare copy)
* Snapshotting and command logging enabled.

Note that:

* All our our contest entries are *ACID transactions*
* All transactions take place on 2 nodes
* We never over enter
* We never under enter
* We didn't get any latency spikes.
* 'z1d.xlarge' has 4 vCPUs, or 2 real ones. Given that we're using k=1 (1 spare copy) our cluster will have a total of 6 CPU cores and will use 2 of them for each and every transaction.

### z1d.xlarge: 16 Contests with 40,000 places each


* 16 Contests
* 3,000,000 customers
* 40,000 entries per contest
* Method: starting from 10K TPS increase throughput in 5K thousands of transactions per second increments until the 95th percentile latency > 5ms.

We were able to scale to 55K TPS (blue line) with a latency of around 3ms (orange line), with an even distribution of contests. The sudden spike at the end is caused by us running out of CPU. 


![16 Contests with 40,000 places each]
(https://bitbucket.org/voltdbseteam/voltdb-fantasy-sports-registration/raw/04bd11bbc882ae82b197efe50d5790824a9eaacc/results/z1d.xlarge_3node_k1_16contests.png)

     
### z1d.xlarge: 1 Contest with 1 million places.



* 1 Contest
* 3,000,000 customers
* 1,000,000 entries per contest
* Method: starting from 10K TPS increase throughput in 5K thousands of transactions per second increments until the 95th percentile latency > 5ms.

The nature of this test means that all the work will be channeled through a single partition in VoltDB.  We steadily increased the load until 95th percentile latency was > 5ms, by which time we were able to register 55K entries per second. 



![1 Contest with 1,000,000 place]
(https://bitbucket.org/voltdbseteam/voltdb-fantasy-sports-registration/raw/425eb541321258e21b626acf83d2f0ca65ebe2bb/results/z1d.xlarge_3node_k1_1contest.png)

### z1d.2xlarge: 16 Contests with 40,000 places each


* 16 Contests
* 3,000,000 customers
* 40,000 entries per contest
* Method: starting from 10K TPS increase throughput in 5K thousands of transactions per second increments until the 95th percentile latency > 5ms.

We were able to scale to 640K TPS (blue line) with a latency of around 3ms (orange line), with an even distribution of contests. The sudden spike at the end is caused by us running out of CPU. 


![16 Contests with 40,000 places each]
(https://bitbucket.org/voltdbseteam/voltdb-fantasy-sports-registration/raw/0b3ec9df0963b83cd39d6df6a285aabbc0d06ce3/results/z1d.2xlarge_3node_k1_16contests.png)

     
### z1d.2xlarge: 1 Contest with 1 million places.



* 1 Contest
* 3,000,000 customers
* 1,000,000 entries per contest
* Method: starting from 10K TPS increase throughput in 5K thousands of transactions per second increments until the 95th percentile latency > 5ms.

The nature of this test means that all the work will be channeled through a single partition in VoltDB.  We steadily increased the load until 95th percentile latency was > 5ms, by which time we were able to register 105K entries per second. 



![1 Contest with 1,000,000 place]
(https://bitbucket.org/voltdbseteam/voltdb-fantasy-sports-registration/raw/0b3ec9df0963b83cd39d6df6a285aabbc0d06ce3/results/z1d.2xlarge_3node_k1_1contest.png)





