package server;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import org.voltdb.SQLStmt;
import org.voltdb.VoltProcedure;
import org.voltdb.VoltTable;
import org.voltdb.types.TimestampType;

/**
 * This class is a VoltDB stored procedure for registering players in a 
 * hypothetical fantasy sports contest. The 'run' is method is called
 * when the user involves the stored procedure. 
 * 
 * @author drolfe
 *
 */
public class RegisterForCompetition extends VoltProcedure {

    // @formatter:off

    public static final SQLStmt getContest = new SQLStmt("SELECT * FROM contest WHERE contest_id = ?;");
    
    public static final SQLStmt getContestEntryCount = new SQLStmt("SELECT * FROM contest_entry_count WHERE contest_id = ?;");
    
    public static final SQLStmt getExistingContestEntry = new SQLStmt(
            "SELECT * "
            + "FROM contest_entry "
            + "WHERE contest_id = ? AND customer_id = ?;");
    
    public static final SQLStmt enterContest = new SQLStmt("INSERT INTO contest_entry "
            + " (contest_id, customer_id, entrydate) "
            + " VALUES "
            + " (?,?,NOW);");
    
    public static final SQLStmt reportFirstEntry = new SQLStmt("UPDATE contest SET start_register = ? WHERE contest_id = ?;");
    
    public static final SQLStmt reportLastEntry = new SQLStmt("UPDATE contest SET end_register = ? WHERE contest_id = ?;");
    
    // @formatter:on

    // Used to report results
    public static final byte REGISTER_STATUS_ENTERED = 0;
    public static final byte REGISTER_STATUS_FULL = 1;
    public static final byte REGISTER_STATUS_DUPLICATE = 2;
  
    /**
     * Attempt to register a customer in a given contest. 
     * @param contestId
     * @param customerId
     * @return a VoltTable[] of results. We also set AppStatusString to be the contestId
     * @throws VoltAbortException if the contest doesn't exist.
     */
    public VoltTable[] run(long contestId, long customerId) throws VoltAbortException {
        
        // Used so the callback knows which contest we're talking about. For 
        // large quantities of info we'd return VoltTable[] instead...
        this.setAppStatusString(contestId+"");

        // Make sure the contest exists - transaction will rollback if this
        // doesn't return one and only one row.
        this.voltQueueSQL(getContest, EXPECT_ONE_ROW, contestId);

        // See how many people have already signed up
        this.voltQueueSQL(getContestEntryCount, contestId);

        // See if they are trying to enter twice
        this.voltQueueSQL(getExistingContestEntry, contestId, customerId);

        // Ask VoltDB's C++ core to execute the SQL we've enqueued. 
        VoltTable[] firstPass = voltExecuteSQL();

        // See if they have already entered...
        if (firstPass[2].advanceRow()) {
            this.setAppStatusCode(REGISTER_STATUS_DUPLICATE);
            return firstPass;
        }

        // Find capacity of contest...
        firstPass[0].advanceRow();
        long maxEntries = firstPass[0].getLong("max_entrants");

        // Find number of people already signed up...
        long currentEntries = 0;
        if (firstPass[1].advanceRow()) {
            currentEntries = firstPass[1].getLong("current_entries");
        }

        if (currentEntries < maxEntries) {

            // We can enter the contest...
            this.voltQueueSQL(enterContest, contestId, customerId);
            this.voltQueueSQL(getContestEntryCount, contestId);
            this.setAppStatusCode(REGISTER_STATUS_ENTERED);

            if (currentEntries == 0) {
                
                // mark time of first entry
                this.voltQueueSQL(reportFirstEntry, this.getTransactionTime(), contestId);
                
            } else if (currentEntries == (maxEntries - 1)) {
                
                // Mark time of last entry
                this.voltQueueSQL(reportLastEntry, this.getTransactionTime(), contestId);
                
            }

        } else {
            
            this.setAppStatusCode(REGISTER_STATUS_FULL);
            return firstPass;
        }

        return voltExecuteSQL(true);
    }
}
