#!/bin/sh

if 
	[ "$#" -ne 2 -a "$#" -ne 8  ]
then
	echo Usage: $0 hostname filename
	echo or...
	echo Usage: $0 hostname contestcount customercount tpms secondspertest placesineachcontest maxlatency filename

	exit 1
fi


if
	[ "$#" = 2 ]
then
	P_VOLTHOSTS=$1
	P_LOGFILE=$2

	java -jar ../jars/voltdb-fantasy-sports-registration-client.jar  ${P_VOLTHOSTS} | tee -a ${P_LOGFILE}.lst
else
	P_VOLTHOSTS=$1
       	P_CONTESTCOUNT=$2
       	P_CUSTOMERCOUNT=$3
	P_TPMS=$4
	P_SECONDSPERTEST=$5
	P_PLACESINEACHCONTEST=$6
	P_MAXLATENCY=$7	
	P_LOGFILE=$8

	java -jar ../jars/voltdb-fantasy-sports-registration-client.jar  ${P_VOLTHOSTS} \
       	$P_CONTESTCOUNT \
       	$P_CUSTOMERCOUNT \
	$P_TPMS \
	$P_SECONDSPERTEST \
	$P_PLACESINEACHCONTEST \
	$P_MAXLATENCY | tee -a ${P_LOGFILE}.lst


fi

exit 0

