package clientclasses;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import org.voltdb.client.ClientResponse;
import org.voltdb.client.ProcedureCallback;

import server.RegisterForCompetition;

/**
 * Callback that keeps track of how many votes each contest has...
 * 
 * @author drolfe
 *
 */
public class SmarterCountingCallback implements ProcedureCallback {

    /**
     * Thread safe singleton object used to count things.
     */
    CounterCache shc;

    /**
     * This class is a callback for a VoltDB stored procedure. It's goal is
     * to keep track of the outcomes of attempts to register for a fantasy
     * sports contest.
     * 
     * @param shc
     */
    public SmarterCountingCallback(CounterCache shc) {

        this.shc = shc;

    }

    /* (non-Javadoc)
     * @see org.voltdb.client.ProcedureCallback#clientCallback(org.voltdb.client.ClientResponse)
     */
    @Override
    public void clientCallback(ClientResponse arg0) throws Exception {

        try {

            if (arg0.getStatus() != ClientResponse.SUCCESS) {
                
                FantasySportsRegistrationDemo.msg("Error Code " + arg0.getStatusString());
                shc.incCounter("FAIL_" + arg0.getAppStatusString());
                
            } else {

                if (arg0.getAppStatus() == RegisterForCompetition.REGISTER_STATUS_ENTERED) {

                    shc.incCounter("REGISTER_STATUS_ENTERED_" + arg0.getAppStatusString());

                } else if (arg0.getAppStatus() == RegisterForCompetition.REGISTER_STATUS_FULL) {

                    shc.incCounter("REGISTER_STATUS_FULL_" + arg0.getAppStatusString());

                } else if (arg0.getAppStatus() == RegisterForCompetition.REGISTER_STATUS_DUPLICATE) {

                    shc.incCounter("REGISTER_STATUS_DUPLICATE_" + arg0.getAppStatusString());

                } else {

                    shc.incCounter("Unexpected Response: " + arg0.getAppStatus());

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
