package clientclasses;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import java.util.HashMap;

/**
 * 
 * Thread safe singleton object used to count things.
 * 
 * @author drolfe
 *
 */
public class CounterCache {

    private static CounterCache instance = null;

    HashMap<String, Long> theCounterMap = new HashMap<String, Long>();

    protected CounterCache() {
        // Exists only to defeat instantiation.
    }

    public static CounterCache getInstance() {
        if (instance == null) {
            instance = new CounterCache();
        }
        return instance;
    }

    public void reset() {

        synchronized (theCounterMap) {

            theCounterMap = new HashMap<String, Long>();

        }
    }

    public long getCounter(String type) {
        Long l = new Long(0);

        synchronized (theCounterMap) {
            l = theCounterMap.get(type);
            if (l == null) {
                return 0;
            }
        }

        return l.longValue();
    }

    public void setCounter(String type, long value) {

        synchronized (theCounterMap) {
            Long l = theCounterMap.get(type);
            if (l == null) {
                l = new Long(value);

            }
            theCounterMap.put(type, l);
        }

    }

    public void incCounter(String type) {

        synchronized (theCounterMap) {
            Long l = theCounterMap.get(type);
            if (l == null) {
                l = new Long(0);
            }
            theCounterMap.put(type, l.longValue() + 1);
        }

    }

    public void incCounter(String type, long value) {

        synchronized (theCounterMap) {
            Long l = theCounterMap.get(type);
            if (l == null) {
                l = new Long(0);
            }
            theCounterMap.put(type, l.longValue() + value);
        }

    }

    @Override
    public String toString() {
        String data = "";
        synchronized (theCounterMap) {

            data = theCounterMap.toString();
        }

        return data;
    }

}