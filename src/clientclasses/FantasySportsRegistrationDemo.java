package clientclasses;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.voltdb.client.Client;
import org.voltdb.client.ClientConfig;
import org.voltdb.client.ClientFactory;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.ProcCallException;

public class FantasySportsRegistrationDemo {

    private static final int TPMS_STEP = 5;

    public static void main(String[] args) {
        try {

            Client mainClient;
            FantasySportsRegistrationDemo fsdr = new FantasySportsRegistrationDemo();
            String hostString = "localhost";

            if (args.length > 0) {
                hostString = args[0];
            }

            mainClient = connectVoltDB(hostString);

            if (args.length == 7) {

                msg("Custom parameters provided...");

                // How many competitions
                int competitionCount = Integer.parseInt(args[1]);

                // How many customers. We loop if 'howMany' > customers.
                int customerCount = Integer.parseInt(args[2]);

                // How many TP per MS.
                int tpMs = Integer.parseInt(args[3]);

                // How many seconds we spend running 
                int howManySeconds = Integer.parseInt(args[4]);

                // How many places there are in each competition.
                int contestSize = Integer.parseInt(args[5]);

                // max acceptable latency in MS.
                int maxLatencyMS = Integer.parseInt(args[6]);

                fsdr.createCustomers(mainClient, customerCount, tpMs);

                int lastLatency = 0;

                while (lastLatency <= maxLatencyMS) {
                    lastLatency = fsdr.runDemo(mainClient, competitionCount, customerCount, tpMs, howManySeconds,
                            contestSize);
                    tpMs += TPMS_STEP;

                }

            } else {

                msg("Default parameters chosen.");
                msg("We'll do 2 tests:");
                msg("1. 16 contests with 40K places");
                msg("2. 1 contest with 1 million places");

                // How many competitions
                int competitionCount = 16;

                // How many customers. We loop if 'howMany' > customers.
                int customerCount = 3000000;

                // How many TP per MS.
                int tpMs = 10;

                // How long we spend doing this
                int howManySeconds = 40;

                // How many places there are in each competition.
                int contestSize = 40000;

                //
                int maxLatencyMS = 5;

                fsdr.createCustomers(mainClient, customerCount, tpMs);

                int lastLatency = 0;

                while (lastLatency <= maxLatencyMS) {
                    lastLatency = fsdr.runDemo(mainClient, competitionCount, customerCount, tpMs, howManySeconds,
                            contestSize);
                    tpMs += TPMS_STEP;

                }

                msg("Now starting a single, big contest...");
                tpMs = 10;
                competitionCount = 1;
                contestSize = 1000000;
                lastLatency = 0;

                while (lastLatency <= maxLatencyMS) {
                    lastLatency = fsdr.runDemo(mainClient, competitionCount, customerCount, tpMs, howManySeconds,
                            contestSize);
                    tpMs += TPMS_STEP;

                }

            }

            mainClient.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public int runDemo(Client mainClient, int competitionCount, int customerCount, int tpMs, int howManySeconds,
            int contestSize) {

        CounterCache shc = CounterCache.getInstance();
        shc.reset();

        try {

            Random r = new Random(42);

            msg("Competitions=" + competitionCount);
            msg("customerCount=" + customerCount);
            msg("tpms=" + tpMs);
            msg("duration in seconds=" + howManySeconds);
            msg("contest size=" + contestSize);

            // First step: Reset data.
            final String[] deleteStatements = { "DELETE FROM contest;", "DELETE FROM contest_entry;" };

            msg("Delete old records...");

            for (int i = 0; i < deleteStatements.length; i++) {
                msg(deleteStatements[i]);
                mainClient.callProcedure("@AdHoc", deleteStatements[i]);
            }
            msg("Done...");

            // We use this for operations which are normally harmless
            ComplainOnErrorCallback coec = new ComplainOnErrorCallback();
            SmarterCountingCallback smc = new SmarterCountingCallback(shc);

            msg("Create " + competitionCount + " contests...");
            long currentMs = System.currentTimeMillis();
            int tpThisMs = 0;

            for (int i = 0; i < competitionCount; i++) {

                if (tpThisMs++ > tpMs) {

                    while (currentMs == System.currentTimeMillis()) {
                        Thread.sleep(0, 50000);
                    }

                    currentMs = System.currentTimeMillis();
                    tpThisMs = 0;
                }

                mainClient.callProcedure(coec, "contest.insert", i, contestSize, null, null);
            }

            mainClient.drain();
            msg("Done...");

            msg("Starting " + howManySeconds + " seconds of entry attempts at " + tpMs
                    + " thousand transactions per second...");

            final long runStartMs = System.currentTimeMillis();
            int i = 0;
            while (runStartMs + (1000 * howManySeconds) > System.currentTimeMillis()) {

                i++;
                if (tpThisMs++ > tpMs) {

                    while (currentMs == System.currentTimeMillis()) {
                        Thread.sleep(0, 50000);
                    }

                    currentMs = System.currentTimeMillis();
                    tpThisMs = 0;
                }

                int customerId = r.nextInt(customerCount);
                int contestId = r.nextInt(competitionCount);

                mainClient.callProcedure(smc, "RegisterForCompetition", contestId, customerId);

                if (i % (tpMs * 1000) == 0) {
                    msg("On attempt " + i);
                }

            }

            mainClient.drain();
            msg("Done...");

            final float durationS = (System.currentTimeMillis() - runStartMs) / 1000;
            final float tps = i / durationS;
            msg("Effective TPS = " + tps);

            for (int c = 0; c < competitionCount; c++) {
                msg("Entered   contest " + c + ": " + shc.getCounter("REGISTER_STATUS_ENTERED_" + c));
                msg("Full      contest " + c + ": " + shc.getCounter("REGISTER_STATUS_FULL_" + c));
                msg("Duplicate contest " + c + ": " + shc.getCounter("REGISTER_STATUS_DUPLICATE_" + c));
            }

            getContests(mainClient);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int lastLatency = getLatencyMS(mainClient);
        msg("95th percentile latency in ms=" + lastLatency);
        return lastLatency;

    }

    private static Client connectVoltDB(String hostname) throws Exception {
        Client client = null;
        ClientConfig config = null;

        try {
            msg("Logging into VoltDB");

            config = new ClientConfig(); // "admin", "idontknow");
            config.setMaxOutstandingTxns(200000);
            config.setMaxTransactionsPerSecond(800000);
            config.setTopologyChangeAware(true);
            config.setReconnectOnConnectionLoss(true);

            client = ClientFactory.createClient(config);

            String[] hostnameArray = hostname.split(",");

            for (int i = 0; i < hostnameArray.length; i++) {
                msg("Connect to " + hostnameArray[i] + "...");
                try {
                    client.createConnection(hostnameArray[i]);
                } catch (Exception e) {
                    msg(e.getMessage());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("VoltDB connection failed.." + e.getMessage(), e);
        }

        return client;

    }

    public int getLatencyMS(Client mainClient) {

        int latencyMS = Integer.MAX_VALUE;

        try {
            ClientResponse countResponse = mainClient.callProcedure("@Statistics", "LATENCY", 1);

            msg("latency (microseconds):");
            msg(System.lineSeparator() + countResponse.getResults()[0].toFormattedString());

            // We have one row for each node in the cluster. Pick highest...
            long latencyMicroseconds = 0;
            while (countResponse.getResults()[0].advanceRow()) {

                if (countResponse.getResults()[0].getLong("P95") > latencyMicroseconds) {
                    latencyMicroseconds = countResponse.getResults()[0].getLong("P95");
                }
            }

            msg("95th percentile latency in microseconds: " + latencyMicroseconds);
            latencyMS = (int) (latencyMicroseconds / 1000);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return latencyMS;

    }

    public void getContests(Client mainClient) {

        try {
            ClientResponse countResponse = mainClient.callProcedure("GetAllContests");

            msg(System.lineSeparator() + countResponse.getResults()[0].toFormattedString());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void createCustomers(Client mainClient, int customerCount, int tpMs) {

        try {
            ClientResponse countResponse = mainClient.callProcedure("@AdHoc",
                    "SELECT COUNT(*) HOW_MANY FROM CUSTOMER;");

            countResponse.getResults()[0].advanceRow();
            long howMany = countResponse.getResults()[0].getLong("HOW_MANY");

            if (howMany != customerCount) {
                if (howMany > 0) {
                    mainClient.callProcedure("@AdHoc", "DELETE FROM CUSTOMER;");
                }

                msg("Create " + customerCount + " customers...");

                ComplainOnErrorCallback coec = new ComplainOnErrorCallback();
                long currentMs = System.currentTimeMillis();
                int tpThisMs = 0;

                for (int i = 0; i < customerCount; i++) {

                    if (tpThisMs++ > tpMs) {

                        while (currentMs == System.currentTimeMillis()) {
                            Thread.sleep(0, 50000);
                        }

                        currentMs = System.currentTimeMillis();
                        tpThisMs = 0;
                    }

                    mainClient.callProcedure(coec, "customer.insert", i, "Mr. " + i);
                    if (i % (tpMs * 1000) == 1) {
                        msg("On customer " + i);

                    }
                }
                mainClient.drain();
                msg("Done...");
            }

        } catch (IOException | ProcCallException | InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void msg(String message) {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        System.out.println(strDate + ":" + message);
    }

}
